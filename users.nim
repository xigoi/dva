import times
import bakalari

type
  ServiceKind* = enum
    srvBakalariMessageForwarding
    srvBakalariMarkForwarding
    srvBakalariDailyTimetable
  Service* = object
    case kind*: ServiceKind
    of srvBakalariMessageForwarding:
      discard
    of srvBakalariMarkForwarding:
      discard
    of srvBakalariDailyTimetable:
      discard
  UserConfig* = object
    email*: string
    services*: seq[Service]
  UserData* = object
    lastServeTime*: DateTime
  User* = ref object
    name*: string
    config*: UserConfig
    data*: UserData
    bakalari*: Bakalari
